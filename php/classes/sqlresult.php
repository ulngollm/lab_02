<?php

class SqlConnection
{

    private $sqlResult;
    private $connect;

    public function __construct($database)
    {
        $this->connect = mysqli_connect(HOST, USER, PASS, $database) or die(mysqli_error($this->connect));
    }

    private function FormatResult()
    {
        $result = "<table><thead>";
        $flagfirst = true;

        while ($row = $this->sqlResult->fetch_assoc()) {
            if ($flagfirst) {
                $key = array_keys($row);
                $result .= "<tr>";
                for ($i = 0; $i < count($key); $i++) {
                    $result .= "<th>$key[$i]</th>";
                }
                $flagfirst = false;
                $result .= "</tr></thead><tbody>";
            }
            $result .= '<tr>';
            foreach($row as $key => $value){
                $result .=  "<td>$value</td>"; 
            }
            $result .= "</tr>";
        }
        $result .= "</tbody></table>";
        return $result;
    }

    public function GetFormattedResult($query)
    {
        $this->MakeRequest($query);
        return $this->FormatResult();
    }

    public function MakeRequest($query)
    {
        $this->sqlResult = mysqli_query($this->connect, $query) or die(mysqli_error($this->connect));
        return $this->sqlResult;
    }
    
    
}
