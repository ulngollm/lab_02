<?php

class Template
{

    public $values = array();
    public $html;

    public function __construct($tpl_name)
    {
        if(file_exists($tpl_name)){
            $this->html = file_get_contents($tpl_name);
        }
        else die("Не найден файл шаблона $tpl_name");
    }

    public function SetValue($key, $var)
    {
        $this->values["{{$key}}"] =  $var;
    }

    private function TemplateParse()
    {
        foreach($this->values as $entry => $replacer){
            $this->html = str_replace($entry, $replacer, $this->html);
        }
    }

    public function ToString(){
        $this->TemplateParse();
        return $this->html;
    }
}