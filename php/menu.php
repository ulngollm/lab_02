<?php

function isActiveMenuItem($url)
{
    $class = (strpos($_SERVER['REQUEST_URI'], $url) !== false) ? 'link_active' : '';
    return $class;
}
$menu = '<a href="/?page=1" class="link ' . isActiveMenuItem('/?page=1') . '">Номенклатура</a>' .
    '<a href="/?page=2" class="link ' . isActiveMenuItem('/?page=2') . '">Контрагенты</a>' .
    '<a href="/?page=3" class="link ' . isActiveMenuItem('/?page=3') . '">Взаиморасчеты</a>'.
    '<a href="/?page=4" class="link ' . isActiveMenuItem('/?page=4') . '">Остатки</a>'.
    '<a href="/?page=5" class="link ' . isActiveMenuItem('/?page=5') . '">Деньги</a>';
