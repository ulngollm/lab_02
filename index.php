<?php
require_once 'php/classes/template.php';
require_once 'php/model.php';
require_once 'php/pagecontroller.php';
require_once 'php/authcontroller.php';
require_once 'php/menu.php';

$tpl = new Template('templates/index.tpl');
$tpl->SetValue('MENU', $menu);
$tpl->SetValue('TABLE', $sqlConnection->GetFormattedResult($query));
$tpl->SetValue('AUTH', $auth_block);
print($tpl->ToString());
