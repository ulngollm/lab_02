<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Лабораторная работа №3</title>
    <link rel="stylesheet" type="text/css" href="/style/style.css">
    <link rel="stylesheet" type="text/css" href="/style/tstyle_custom.css">
</head>
<body>
<div id="wrapper">
<div id="header">
    <div class="header__title">Лабораторная работа №3</div>
    <div class="header__user-block">
        {AUTH}
    </div>
</div>
<div id="menu">
    {MENU}
</div>
<div id="content">
    {TABLE}
</div>
<div id="footer">Донской Государственный Технический Университет <br> Noname J.S.</div>
</div>
</body>
</html>