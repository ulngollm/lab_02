<?php
require_once 'model.php';


if ($_REQUEST) {
    $mail = trim($_REQUEST['mail']);
    $password = trim($_REQUEST['password']);
    $authResult = authUser($mail, $password, $sqlConnection);
    header("Location: /?$authResult");
}


function authUser($mail, $password, $sqlConnection)
{
    $result = $sqlConnection->MakeRequest("SELECT mail, name, password from user where mail='$mail'");

    if (empty($result->num_rows))
        return 'auth=NOT_EXIST';
    else {
        $row = $result->fetch_assoc();
        if ($row['password'] != md5($password))
            return 'auth=PASS_ERR';
        else {
            session_start();
            $_SESSION['name'] = $row['name'];
            $_SESSION['mail'] = $row['mail'];
            return 'auth=OK';
        }
    };
}
