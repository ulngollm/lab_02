<?php
session_start();
if (isset($_GET['signout'])) {
    session_unset();
}
// $auth_block;
if (empty($_SESSION) && !isset($_GET['auth'])) {
    $auth_tpl = new Template($_SERVER['DOCUMENT_ROOT'] . '/templates/header_form_auth.html');
    $auth_tpl->SetValue('ERROR', '');
    $auth_block = $auth_tpl->ToString();
} 
else if ($_SESSION) {
    $auth_tpl = new Template($_SERVER['DOCUMENT_ROOT'] . '/templates/header_user_block.html');
    $auth_tpl->SetValue('NAME', $_SESSION['name']);
    $auth_tpl->SetValue('EMAIL', $_SESSION['mail']);
    $auth_block = $auth_tpl->ToString();
} 
else if (isset($_GET['auth'])) {
    switch ($_GET['auth']) {
        case 'NOT_EXIST':
            $auth_error = "Пользователь не найден";
            break;
        case 'PASS_ERR':
            $auth_error = "Неверный пароль";
            break;
        default:
        case 'OK':
            $auth_error = "";
            break;
    }
    $auth_tpl = new Template($_SERVER['DOCUMENT_ROOT'] . '/templates/header_form_auth.html');
    $auth_tpl->SetValue('ERROR', $auth_error);
    $auth_block = $auth_tpl->ToString();
}
